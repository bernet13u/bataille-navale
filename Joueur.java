
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Joueur implements Serializable {
	/**
	 * parametre grilleBat et grilleTir correspondent aux grille de placement de bateaux et de tir du joueur
	 */
	private Grille grilleBat, grilleTir;
	/**
	 * parametre lb represente une liste de bateaux
	 */
	private ListeBateaux lb;
	/**
	 * parametre bateauxDetruits permet de savoir ou en est la partie, quand 5 bateaux sont detruits, la flotte entiere est coulee
	 */
	private int bateauxDetruits;
	/**
	 * Constructeur Joueur permet de creer un joueur avec ses grilles et sa liste de bateaux ainsi que son compteur de bateaux detruits
	 * @param gBat grille de bateau du joueur
	 * @param gTir grille de tir du joueur
	 */
	public Joueur(Grille gBat, Grille gTir) {
		this.grilleBat = new Grille(gBat);
		this.grilleTir = new Grille(gTir);
		this.lb = new ListeBateaux();
		this.bateauxDetruits = 0;
	}
	
	/**
	 * Methode ajouterBateau permet de poser un bateau sur la grille, on prends en compte le fait quun bateau soit deja dans la case ou que le bateau ne soit pas hors de la grille.
	 * On choisit une position horizontale ou verticale
	 */
	public void ajouterBateau() throws BateauException {
		Scanner sc = new Scanner(System.in);
		this.lb.triParTaille();
		for (int k = 0; k < lb.getListeBateaux().size(); k++) {
			Bateau b = lb.getListeBateaux().get(k);
			int tailleBat = lb.getListeBateaux().get(k).getLongueur();
			boolean res = true;
			System.out.println("\nPlacez le bateau suivant : ");
			System.out.println(b.toString());
			System.out.println("Entrez les coordonees (x puis y) :");
			int x = sc.nextInt();
			int y = sc.nextInt();
			System.out.println("Entrez une orientation (H ou V) :");
			sc = new Scanner(System.in);
			String orientation = sc.nextLine();
			
			if (orientation.equals("V")||orientation.contentEquals("v")) {
				if (y + tailleBat > grilleBat.getTaille()) {
					System.out.println("Placement impossible(Hors grille)");
				}
				for (int i = y; i < y + tailleBat; i++) {
					if(grilleBat.getGrille()[x - 1][i - 1].getABateau()) {
						res = false;
					} 
				}
				if(res ==true) {
					for (int i = y; i < y + tailleBat; i++) {
						grilleBat.getGrille()[x - 1][i - 1].setVal("o");
						grilleBat.getGrille()[x - 1][i - 1].setBateau(b);
					}
				}else {
					k-=1;
					System.out.println("Bateau deja place ici");
				}
				grilleBat.afficherGrille();
			} 
			if (orientation.equals("H")||orientation.contentEquals("h")) {
				if (x + tailleBat > grilleBat.getTaille()) {
					System.out.println("Placement impossible(Hors grille)");
				}
				for (int j = x; j < x + tailleBat; j++) {
					if(grilleBat.getGrille()[j-1][y-1].getABateau()) {
						res = false;
						
					} 
				}
				if(res == true) {
					for (int j = x; j < x + tailleBat; j++) {
						grilleBat.getGrille()[j - 1][y - 1].setVal("o");
						grilleBat.getGrille()[j - 1][y - 1].setBateau(b);
					}
					
				}else {
					k-=1;
					System.out.println("Bateau deja place ici");
				}
				
			}
			grilleBat.afficherGrille();
			}
		
	}
	
	/**
	 * Methode tirer visant une case et un potentiel bateau, si il y en a un, on lui retire des points de vie, change la valeur de la case
	 * @param grilleTir
	 */
	public void tirer(Grille grilleTir) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Choisisser une cible (x puis y)");
		int x = sc.nextInt();
		int y = sc.nextInt();
		int bateauxRestants =0;
		if(this.grilleTir.getGrille()[x-1][y-1].getVal()=="X") {
			System.out.println("Bateau touche");			
		}
		if(this.grilleBat.getGrille()[x-1][y-1].getVal()=="o") {
			this.grilleBat.getGrille()[x-1][y-1].subirDegats();
			if(this.grilleBat.getGrille()[x-1][y-1].getBateau().getPourcentageDegats()==1) {
				this.bateauxDetruits +=1;
				bateauxRestants = this.lb.getListeBateaux().size()-bateauxDetruits;
				System.out.println("Bateau coule, "+ bateauxRestants +" bateaux restants.");	
			
			} else {
			System.out.println("Bateau touche");
			}
			grilleTir.getGrille()[x-1][y-1].setVal("X");
		} else if(this.grilleBat.getGrille()[x-1][y-1].getVal()=="."){
			System.out.println("Rate, reessayer");
			grilleTir.getGrille()[x-1][y-1].setVal("+");
		} 
		
	}
	
	
	/**
	 * Methode getBateauxDetruits donne acces au nombre de bateaux detruits et donc a l'avancement de la partie
	 * @return int, le nombre de bateaux actuellement detruits
	 */
	public int getBateauxDetruits() {
		return this.bateauxDetruits;
	}
	
	/**
	 * Methode getListeBateau donne acces a la liste de bateau du joueur
	 * @return ListeBateaux, la liste du joueur
	 */
	public ListeBateaux getListeBateau() {
		return this.lb;
	}
	/**
	 * Methode sauve permet de sauver les donnees accumulees
	 * @param nomfich nom du fichier destination
	 */
	public void sauve(String nomfich) {
		try {
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(nomfich));
			oos.writeObject(grilleBat);
			oos.writeObject(grilleTir);
			oos.writeObject(lb);
			oos.writeObject(bateauxDetruits);
			oos.close();
		}catch(IOException e) {
			System.out.println("erreur d'E/S");
			e.printStackTrace();
		}
	}
	/**
	 * Methode charge permet de recuperer les donnees sauvee dans un fichier
	 * @param nomfich fichier source dinformations
	 * @throws ClassNotFoundException
	 */
	public void charge(String nomfich) throws ClassNotFoundException {
		try {
			ObjectInputStream ois =new ObjectInputStream(new FileInputStream(nomfich));
			grilleBat = (Grille)(ois.readObject());
			grilleTir = (Grille)(ois.readObject());
			lb = (ListeBateaux)(ois.readObject());
			bateauxDetruits = (int)(ois.readObject());
			ois.close();
		}catch(IOException e) {
			System.out.println("erreur d'E/S");
			e.printStackTrace();
		}
	}
}
