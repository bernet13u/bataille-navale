import java.io.Serializable;

public class Case implements Serializable {
	/**
	 * Parametre bateau, fait reference au bateau se trouvant dans la case
	 */
	private Bateau bateau;
	/**
	 * parametre booleen aBateau indiquant si un bateau se trouve dans la case
	 */
	private boolean aBateau;
	/**
	 * parametres x et y, entier font reference a la position de la case dans la grille
	 */
	private int x, y;
	/**
	 * parametre aEuImpact, booleen indique si la case a subi un tir ou non
	 */
	private boolean aEuImpact;
	/**
	 * parametre value, chaine de caractere permet d'indentifier ce qui a ete fait sur une case (. si rien, + si tir, o si bateau, X si bateau touche)
	 */
	private String value;

	
	/**
	 * Constructeur Case permet de creer une case avec une position, une valeur . et sans bateau ni tir subit
	 * @param x entier, position sur l'axe des abscisses
	 * @param y entier, position sur l'axe des ordonnees
	 */
	public Case(int x, int y) {
		this.x = x;
		this.y = y;
		this.aEuImpact = false;
		this.aBateau = false;
		this.value = ".";
	}

	/**
	 * m�thode subirDegats permet de modifier la valeur dune case en fonction de la valeur du parametre aBateau et baisse la vie de ce bateau
	 */
	public void subirDegats() {
		
		if(this.aBateau) {
			this.bateau.setVie(-1);
			setVal("X");
		}else
			setVal("+");
		this.aEuImpact=true;
		
	}
	/**
	 * m�thode toString affiche la valeur de la case
	 * @return String, valeur de la case
	 */
	public String toString() {
		return this.value;
	}
	/**
	 * m�thode getBateau, permet de recuperer le bateau se trouvant dans la case
	 * @return Bateau, le bateau se trouvant dans la case
	 */
	public Bateau getBateau() {
		return this.bateau;
	}
	/**
	 * m�thode getABateau indique si la case contient un bateau ou non
	 * @return booleen, indiquant si la case est occupee par un bateau
	 */
	public boolean getABateau() {
		return this.aBateau;
	}
	/**
	 * m�thode getVal donne acces a la valeur de la case
	 * @return String, valeur de la case
	 */
	public String getVal() {
		return this.value;
	}
	/**
	 * methode setVal permet dattribuer une valeur a une case
	 * @param val int, permet didentifier ce qui se trouve dans la case
	 */
	public void setVal(String val) {
		this.value = val;
		if(val=="o") {
			this.aBateau = true;
		if(val =="X")
			this.aEuImpact =true;
	}
	
}
	/**
	 * m�thode setbateau modifie la valeur du parametre bateau
	 * @param b indique le bateau qui occuepra cette case
	 */
	public void setBateau(Bateau b){
		this.bateau = b;
		this.aBateau = true;
	}

}
