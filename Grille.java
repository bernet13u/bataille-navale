
import java.io.Serializable;
import java.util.InputMismatchException;

public class Grille implements Serializable {
	/**
	 * Parametre taille correspondant a la taille de la grille
	 */
	private int taille;
	/**
	 * parametre tabGrille, tableau de Case representant la grille
	 */
	private Case tabGrille[][];
	/** 
	 * Constructeur Grille, construit un tableau de Case dont la taille est limitee entre 10 et 250
	 * @param t entier, taille de la grille voulue 
	 */
	public Grille(int t) {
			if (t < 10) {
				t = 10;
			}
			if (t > 250) {
				t = 250;
			}
			this.taille = t;
			this.tabGrille = new Case[this.taille][this.taille];
			
			for (int i = 0; i < t; i++) {
				for (int j = 0; j < t; j++) {
					this.tabGrille[i][j] = new Case(i, j);
				}
			}
		
	}
	
	/**
	 * Constructeur avec parametre constuit une grille similaire a celle passee en parametre.
	 * Utile lors de la creation des grille de joueur
	 * @param g Grille, grille dont on veut copier les caracteristiques
	 */
	public Grille(Grille g) {
		this.taille = g.getTaille();
		this.tabGrille = g.getGrille();
	}
	/**
	 * M�thode utile pour afficher les grilles lors du lancement du jeu, permet de visualiser les objets places
	 */
	public void afficherGrille() {
		System.out.println(" ");
		for (int i = 0; i <this.taille; i++) {
			for (int j = 0; j < this.taille; j++) {
				System.out.print(this.tabGrille[j][i].toString() + " ");
			}
			System.out.println();
		}
	}
	
	
	/**
	 * M�thode permettant de recuperer la taille de la grille
	 * @return entier, taille de la grille en question
	 */
	public int getTaille() {
		return this.taille;
	}
	/**
	 * M�thode permettant de recuperer un tableau de Case representant la grille
	 * @return Tableau de cases, permet dappliquer les methodes sur des cases
	 */
	public Case[][] getGrille() {
		return this.tabGrille;
	}

	
}