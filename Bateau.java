
import java.io.Serializable;
import java.util.Comparator;

public class Bateau implements Comparable<Bateau>, Serializable{
	/**
	 * Parametre nom identifiant le bateau
	 */
	private String nom;
	/**
	 * Parametre longueur indiquant la longueur du bateau
	 */
	private int longueur;
	/**
	 * parametre vie indiquant la vie du bateau
	 */
	private int vie;
	/**
	 * parametre pourcentageDegats indiquant le poucentage de degats subit par un navire
	 */
	private double pourcentageDegats;
	
	/**
	 * Constructeur Bateau donnant un nom, une longueur, de la vie et un pourcentage de degats subit nul a un bateau
	 * @param l longueur du bateau
	 * @param n nom du bateau
	 */
	public Bateau(int l, String n) {
		this.pourcentageDegats = 0;
		this.nom = n;
		this.longueur = l;
		this.vie = l;
	}


	/**
	 * Methode toString permettant dafficher les caracteristiques dun bateau
	 */
	public String toString() {
		String res = "	- Bateau de type : " + this.nom + ", de taille " + this.longueur + " ayant " + this.vie
				+ " de vie." + "\n";
		return res;
	}
	

	/**
	 * methode getLongueur, affiche lattribut vie dun bateau
	 * @return longueur, la longueur du bateau
	 */
	public int getLongueur() {
		return this.longueur;
	}
	/**
	 * Methode getNom donne acces au nom dun bateau
	 * @return nom, une chaine de caractere correspondant au nom du bateau
	 */
	public String getNom() {
		return this.nom;
	}
	
	/**
	 * Methode setVie permet de modifier lattribut vie dun bateau
	 * @param vie quantite de vie a ajouter, peut etre negatif
	 */
	public void setVie(int vie) {
		this.vie += vie;
	}
	/**
	 * Methode getVie donne acces a la vie dun bateau
	 * @return vie, la vie du bateau
	 */
	public int getVie() {
		return this.vie;
	}
	/**
	 * Methode getPourcentageDegats donne acces au pourcentage de degats subit pas un bateau
	 * @return pourcentagedegats, la quantite de degats subit en pourcentage(entre 0 et 1)
	 */
	public double getPourcentageDegats() {
		this.pourcentageDegats = (this.longueur - this.vie) / this.longueur;
		return this.pourcentageDegats;
	}
	
	@Override
	public int compareTo(Bateau bat) {
		return this.longueur - bat.getLongueur();
	}
	/**
	 * Methode compareToVie permet de comparer la vie dun bateau a celle dun autre
	 * @param bat la vie du bateau actuel sera comparee a celle de ce bateau
	 * @return int, la difference de vien entre les deux bateaux
	 */
	public int compareToVie(Bateau bat) {
		return this.vie - bat.getVie();
	}
	
	
}
