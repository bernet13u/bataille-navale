
import java.util.*;

public class Principale {
	public static void main(String[] args) throws Exception {
		Scanner sc = new Scanner(System.in);
		System.out.println("Bienvenue dans cette Bataille Navale");

		
		
		
		System.out.println("\nEntrez 0 pour commencer le jeu de z�ro");
		System.out.println("Entrez 1 pour charger une partie d�ja commenc�e");
		sc = new Scanner(System.in);
		int affichage = sc.nextInt();
		ListeBateaux l = new ListeBateaux();
		switch(affichage) {
		case 0:
			System.out.println("DEBUT DU JEU\n");
			System.out.print("Entrez la taille de la grille voulu : ");
			
			int tailleGrille = 0;
			
			
			try {
				tailleGrille = sc.nextInt();
				}catch(InputMismatchException e2) {
					System.out.println("Valeur incorrecte, valeur par defaut (10) attribuee");
					tailleGrille = 10;
				}
				if (tailleGrille < 10) {
					System.out.println("Taille de la grille trop petite..");
					System.out.println("Grille initialisee a� 10");
				}
				if (tailleGrille > 250) {
					System.out.println("Taille de la grille trop grande..");
					System.out.println("Grille initialisee a� 1000");
				}
				
				Grille grilleBateau = new Grille(tailleGrille);
				Grille grilleTir = new Grille(tailleGrille);
				Joueur j1 = new Joueur(grilleBateau, grilleTir);
				
				System.out.println("Voici votre grille de bateaux : \n");
				grilleBateau.afficherGrille();
				System.out.println("\nPlacez vos bateaux : \n");
				
					j1.ajouterBateau();
				
				System.out.println("Voici votre grille de tirs :");
				grilleTir.afficherGrille();
				int menu = -1;
			
				while(j1.getBateauxDetruits()!=5){
					System.out.println("\nEntrez 0 pour tirer");
					System.out.println("Entrez 1 pour afficher votre liste de bateaux triee par taille");
					System.out.println("Entrez 2 pour afficher votre liste de bateaux triee par vie");
					System.out.println("Entrez 3 pour sauvegarder votre progression");
					menu = sc.nextInt();
					if(menu == 0) {
						try {
						j1.tirer(grilleTir);
						}catch( InputMismatchException e1) {
							System.out.println("Caractere Incorrect");
						}catch( ArrayIndexOutOfBoundsException e2) {
							System.out.println("Tir Hors Grille");
						}
					}
					else if(menu == 1) {
						j1.getListeBateau().triParTaille();
						System.out.println("Liste triee par taille de bateaux");
						for(int i =0; i<j1.getListeBateau().getListeBateaux().size();i++) {
							System.out.println(j1.getListeBateau().getListeBateaux().get(i).toString());
						}
					}else if(menu == 2) {
						j1.getListeBateau().triParVie();
						System.out.println("Liste triee par vie de bateaux");
						for(int i =0; i<j1.getListeBateau().getListeBateaux().size();i++) {
							System.out.println(j1.getListeBateau().getListeBateaux().get(i).toString());
						}
					}else if(menu == 3) {
						j1.sauve("Save.txt");
						System.out.println("Sauvegarde effectuee");
					}
					
					grilleTir.afficherGrille();
				}
				System.out.println("VICTOIRE, fin de partie...");
				
			break;
		case 1:
			int tailleGrille2 =0;
			ListeBateaux l2 = new ListeBateaux();
			Grille grilleBateau2 = new Grille(tailleGrille2);
			Grille grilleTir2 = new Grille(tailleGrille2);
			Joueur j2 = new Joueur(grilleBateau2, grilleTir2);
			
			j2.charge("Save.txt");
			System.out.println("TRAVAIL NON ABBOUTI CONCERNANT LA CHARGE DE SAUVEGARDE, MISE A JOUR A VENIR");
			break;
		}
		
		

		
		
	
		
		
		
		
	}
}
