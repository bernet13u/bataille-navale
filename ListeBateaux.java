
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

public class ListeBateaux implements Serializable {
	/**
	 * Parametre ListeBateaux de type ArrayList correspond a la liste de Bateaux
	 */
	private ArrayList<Bateau> ListeBateaux;
	
	/**
	 * Constructeur de ListeBateaux, remplit la lArrayList attribut de 5 bateaux de taille variees
	 */
	public ListeBateaux() {
		this.ListeBateaux = new ArrayList<Bateau>(5);
		Bateau b1 = new Bateau(2, "Torpilleur");
		Bateau b2 = new Bateau(3, "Sous-Marin");
		Bateau b3 = new Bateau(3, "Contre-torpilleur");
		Bateau b4 = new Bateau(4, "Croiseur");
		Bateau b5 = new Bateau(5, "Porte-Avion");
		this.ListeBateaux.add(b1);
		this.ListeBateaux.add(b5);
		this.ListeBateaux.add(b4);
		this.ListeBateaux.add(b2);
		this.ListeBateaux.add(b3);
	}
	/**
	 * Methode de tri par taille, permet dafficher les bateau triee par taille
	 */
	public void triParTaille() {
		Collections.sort(ListeBateaux,new TriTaille());
	}
	/**
	 * Methode rangeant la liste de bateaux par points de vie croissant
	 */
	public void triParVie() {
		Collections.sort(ListeBateaux,new TriVie());
	}
	
	
	/**
	 * Methode getListeBateaux donne acces a la liste de bateaux cree
	 * @return ArrayList de bateau correspondant a la flotte cree
	 */
	public ArrayList<Bateau> getListeBateaux() {
		return this.ListeBateaux;
	}

	
	


}
