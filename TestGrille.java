import static org.junit.Assert.*;

import org.junit.Test;

public class TestGrille {
	
	/**
	 * Test sur le constructeur ayant un parametre entier de grille avec une taille normale, on verifie que la taille soit la bonne
	 */
	@Test
	public void testConstructeurNormal() {
		int t = 10;
		Grille g = new Grille(t);
		assertEquals("la Grille devrait avoir une taille de 10", 10, g.getTaille());
	}
	/**
	 * Test sur le constructeur ayant un parametre entier de grille avec une taille negative, on verifie que la taille soit la bonne
	 */
	@Test
	public void testConstructeurNeg() {
		int t = -5;
		Grille g = new Grille(t);
		assertEquals("la Grille devrait avoir une taille de 10", 10, g.getTaille());
	}
	/**
	 * Test sur le constructeur ayant un parametre entier de grille avec une taille tres grande, on verifie que la taille soit la bonne
	 */
	@Test
	public void testConstructeurSup() {
		int t = 10000;
		Grille g = new Grille(t);
		assertEquals("la Grille devrait avoir une taille de 250", 250, g.getTaille());
	}
	/**
	 * Test verifiant que le constructeur prenant en parametre une grille soit bien fonctionnel
	 */
	@Test
	public void testConstructeurCloneGrille() {
		int t = 10;
		Grille g = new Grille(t);
		Grille g2 = new Grille(g);
		assertEquals("la Grille 2 devrait avoir une taille de 10", 10, g2.getTaille());
	}
	
	
}
